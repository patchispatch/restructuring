extends Control

class_name DayManager


@onready var objective_count = $ObjectiveCount
@onready var submit_button = $SubmitButton
@onready var pb = $Pb


@export var daily_objective: int

var selected_profiles := []


func _ready():
	objective_count.objective = daily_objective
	submit_button.pressed.connect(_on_submit_button_pressed)


func _on_dialog_box_dialog_started(dialog_code: String) -> void:
	set_ui_visibility(false)


func _on_dialog_box_dialog_finish(dialog_code: String) -> void:
	act_on_dialog_finish(dialog_code)
	set_ui_visibility(true)


func _on_profile_manager_profile_selected(profile_id: String) -> void:
	selected_profiles.append(profile_id)
	act_on_profile_selected(profile_id)


func _on_profile_manager_profile_deselected(profile_id: String) -> void:
	selected_profiles.erase(profile_id)
	act_on_profile_deselected(profile_id)


func _on_submit_button_pressed() -> void:
	act_on_submit_button_pressed()


func act_on_profile_selected(profile_id: String) -> void:
	objective_count.count += 1


func act_on_profile_deselected(profile_id: String) -> void:
	objective_count.count -= 1


func act_on_dialog_finish(dialog_code: String) -> void:
	pass


func act_on_submit_button_pressed() -> void:
	pass


func check_day_finish_conditions() -> void:
	pass


func load_profile_batch(pb_code: String) -> void:
	pb.position = Vector2(80, 140)
	pb.profile_selected.connect(_on_profile_manager_profile_selected)
	pb.profile_deselected.connect(_on_profile_manager_profile_deselected)
	pb.visible = true


func load_dialog(dialog_code: String) -> void:
	var dialog_box = ResourceLoader.load("res://scenes/dialog_box.tscn").instantiate()
	dialog_box.position = Vector2(80, 400)
	dialog_box.dialog_started.connect(_on_dialog_box_dialog_started)
	dialog_box.dialog_finished.connect(_on_dialog_box_dialog_finish)
	dialog_box.set_dialog_id(dialog_code)
	add_child(dialog_box)


func set_ui_visibility(is_visible: bool):
	objective_count.visible = is_visible
	submit_button.visible = is_visible

