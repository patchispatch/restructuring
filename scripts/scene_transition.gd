extends CanvasLayer


const DURATION := 10.0
const MAIN_PATH := "res://scenes/main.tscn"


@onready var music_timer = $MusicTimer
@onready var animation_player = $AnimationPlayer
@onready var transition_rect = $SceneTransitionRect


@export var day_to_load := "res://scenes/day_1.tscn"


var music_bus_id: int
var background_bus_id: int
var original_music_volume: float


# Called when the node enters the scene tree for the first time.
func _ready():
	music_bus_id = AudioServer.get_bus_index("Music")
	background_bus_id = AudioServer.get_bus_index("Background")
	original_music_volume = AudioServer.get_bus_volume_db(music_bus_id)
	music_timer.wait_time = DURATION


func _process(_delta):
	if not music_timer.is_stopped():
		_fade_music()


func _fade_music() -> void:
	AudioServer.set_bus_volume_db(music_bus_id, lerp(-80.0, original_music_volume, music_timer.get_time_left() / DURATION))
	AudioServer.set_bus_volume_db(background_bus_id, lerp(-80.0, original_music_volume, music_timer.get_time_left() / DURATION))

func change_scene(target: String) -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	visible = true
	animation_player.play("fade")
	music_timer.start()
	await animation_player.animation_finished
	
	day_to_load = target
	get_tree().change_scene_to_file(MAIN_PATH)
	visible = false


func change_to_target(target: String) -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	visible = true
	animation_player.play("fade")
	music_timer.start()
	await animation_player.animation_finished
	
	day_to_load = target
	get_tree().change_scene_to_file(target)
	visible = false

