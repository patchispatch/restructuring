extends DayManager

@export var objective := 1

@onready var music := $Music
@onready var suspense_1 := $Suspense1
@onready var suspense_2 := $Suspense2
@onready var shout_1 := $Shout1
@onready var shout_2 := $Shout2
@onready var shout_loop := $ShoutLoop

const SUSPENSE_TIME_1 := 1.5
const SUSPENSE_TIME_2 := 3
const SHOUT_TIME_1 := 3
const SHOUT_TIME_2 := 5

var vp: Viewport
var center_pos: Vector2
var submit_pos: Vector2


# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().root.size_changed.connect(_on_viewport_size_change)
	super()
	Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	vp = get_viewport()
	center_pos = Vector2(vp.size.x/2, vp.size.y/2)
	submit_pos = Vector2(vp.size.x - 50, vp.size.y - 25)
	suspense_1.wait_time = SUSPENSE_TIME_1
	suspense_2.wait_time = SUSPENSE_TIME_2
	shout_1.wait_time = SHOUT_TIME_1
	shout_2.wait_time = SHOUT_TIME_2
	shout_loop.wait_time = SHOUT_TIME_2
	
	objective_count.objective = objective
	objective_count.visible = false
	submit_button.visible = false
	load_dialog("day_4_welcome")


func _on_viewport_size_change() -> void:
	vp = get_viewport()
	center_pos = Vector2(vp.size.x/2, vp.size.y/2)
	submit_pos = Vector2(vp.size.x - 50, vp.size.y - 25)


func act_on_profile_selected(profile_id: String) -> void:
	super(profile_id)
	MouseSpriteController.stop_mouse_automove()
	stop_everything()
	suspense_2.start()


func act_on_profile_deselected(profile_id: String) -> void:
	super(profile_id)
	
	# React for specific profile
	load_dialog("day_4_dots")
	stop_everything()
	MouseSpriteController.stop_mouse_automove()
	pass


func _on_suspense_1_timeout() -> void:
	load_dialog("day_4_difficult")


func _on_suspense_2_timeout() -> void:
	MouseSpriteController.start_mouse_automove(submit_pos)
	shout_2.start()


func _on_shout_1_timeout() -> void:
	load_dialog("day_4_shout_1")


func _on_shout_2_timeout() -> void:
	load_dialog("day_4_shout_2")


func _on_shout_loop_timeout() -> void:
	load_dialog("day_4_shout_loop")


func act_on_dialog_finish(dialog_code: String) -> void:
	match dialog_code:
		"day_4_welcome":
			load_profile_batch("day_4_pb")
			suspense_1.start()
		"day_4_difficult":
			music.play()
			MouseSpriteController.start_mouse_automove(center_pos)
			AudioServer.set_bus_mute(AudioServer.get_bus_index("Background"), true)
			shout_1.start()
		"day_4_dots":
			pass
		"day_4_shout_1":
			pass
		"day_4_shout_2":
			shout_loop.start()
		"day_4_shout_loop":
			pass
		"day_4_end":
			SceneTransition.change_to_target("res://scenes/credits.tscn")


func act_on_submit_button_pressed() -> void:
	if objective_count.count >= objective:
		music.stop()
		stop_everything()
		MouseSpriteController.stop_mouse_automove()
		remove_child(submit_button)
		remove_child(pb)
		remove_child(objective_count)
		load_dialog("day_4_end")
	else:
		load_dialog("day_4_objective_missing")


func stop_everything() -> void:
	suspense_1.stop()
	suspense_2.stop()
	shout_1.stop()
	shout_2.stop()
	shout_loop.stop()


func load_profile_batch(pb_code: String) -> void:
	pb.position = Vector2(center_pos.x - 70, center_pos.y - 100)
	pb.profile_selected.connect(_on_profile_manager_profile_selected)
	pb.profile_deselected.connect(_on_profile_manager_profile_deselected)
	pb.visible = true
