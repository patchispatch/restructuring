extends DayManager

@export var objective := 3

var view_bernier_dialog := false
var view_mosto_dialog := false
var view_jose_dialog := false
var view_gemma_dialog := false

# Called when the node enters the scene tree for the first time.
func _ready():
	super()
	objective_count.objective = objective
	objective_count.visible = false
	submit_button.visible = false
	load_dialog("day_2_welcome")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func act_on_profile_selected(profile_id: String) -> void:
	super(profile_id)
	
	# React for specific profile
	match profile_id:
		"02_01_1":
			if not view_bernier_dialog:
				view_bernier_dialog = true
				load_dialog("day_2_bernier")
		"02_01_2":
			if not view_jose_dialog:
				view_jose_dialog = true
				load_dialog("day_2_jose")
		"02_01_3":
			if not view_gemma_dialog:
				view_gemma_dialog = true
				load_dialog("day_2_gemma")
		"02_01_4":
			if not view_mosto_dialog:
				view_mosto_dialog = true
				load_dialog("day_2_mosto")



func act_on_profile_deselected(profile_id: String) -> void:
	super(profile_id)
	
	# React for specific profile
	pass


func act_on_dialog_finish(dialog_code: String) -> void:
	match dialog_code:
		"day_2_welcome":
			load_profile_batch("day_2_pb")
		"day_2_end":
			check_on_finish_conditions()


func act_on_submit_button_pressed() -> void:
	if objective_count.count >= objective:
		load_dialog("day_2_end")
	else:
		load_dialog("objective_missing")


func check_on_finish_conditions() -> void:
	if selected_profiles.has("02_01_4"):
		SceneTransition.change_scene("res://scenes/day_4.tscn")
	else:
		SceneTransition.change_scene("res://scenes/day_3.tscn")
