extends Control


@export var text_speed: float

var dialog_id: String
var dialog_path: String
var dialog
var sentence_num := 0
var finished := false

@onready var character = $DialogBox/Person
@onready var dialogue = $DialogBox/TextMargin/Text
@onready var timer = $Timer
@onready var key_press_sound = $KeyPress
@onready var spacebar_press_sound = $SpacebarPress


signal dialog_started(dialog_id: String)
signal dialog_finished(dialog_id: String)

func set_dialog_id(id: String) -> void:
	dialog_id = id

# Called when the node enters the scene tree for the first time.
func _ready():
	dialog_path = get_dialog_path(dialog_id)
	timer.wait_time = text_speed
	dialog = get_dialog()
	assert(dialog, "Dialog not found")
	MouseSpriteController.is_dialog_showing = true
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	dialog_started.emit(dialog_id)
	next_sentence()

func _process(_delta):
	if Input.is_action_just_pressed("left_click"):
		if finished: 
			next_sentence()
		else:
			dialogue.visible_characters = len(dialogue.text)

func get_dialog_path(dialog_code: String) -> String:
	return "res://resources/dialogues/%s.json" % dialog_code

func get_dialog() -> Array:
	assert(FileAccess.file_exists(dialog_path), "File path does not exist")
	
	var file := FileAccess.open(dialog_path, FileAccess.READ)
	var json_file := JSON.new()
	var parse_result := json_file.parse(file.get_as_text())

	if parse_result == OK and typeof(json_file.get_data()) == TYPE_ARRAY:
		return json_file.get_data()
	else:
		return []

func next_sentence() -> void:
	
	if sentence_num >= len(dialog):
		# Clear dialog box
		MouseSpriteController.is_dialog_showing = false
		queue_free()
		dialog_finished.emit(dialog_id)
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		return
	
	spacebar_press_sound.play()
	finished = false

	character.text = dialog[sentence_num]["Name"]
	dialogue.bbcode_text = dialog[sentence_num]["Text"]
	
	dialogue.visible_characters = 0
	
	while dialogue.visible_characters < len(dialogue.text):
		dialogue.visible_characters += 1
		if dialogue.visible_characters % 6 == 0:
			key_press_sound.play()
		timer.start()
		await timer.timeout
	
	finished = true
	sentence_num += 1
