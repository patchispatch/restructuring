extends Node

@export var arrow_sprite: Resource
@export var loading_sprite: Resource

@onready var timer = $Timer

var rng = RandomNumberGenerator.new()
var mouse_interpolation := 0.0
var is_mouse_automoving := false
var automove_pos := Vector2.ZERO
var is_dialog_showing := false

# Called when the node enters the scene tree for the first time.
func _ready():
	# Set cursor as arrow
	Input.set_custom_mouse_cursor(arrow_sprite)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if is_mouse_automoving:
		interpolate_mouse_pos(delta)

func interpolate_mouse_pos(delta):
	mouse_interpolation += 0.01 * delta
	var vp = get_viewport()
	Input.warp_mouse(vp.get_mouse_position().lerp(automove_pos, mouse_interpolation))
	
func start_mouse_automove(pos: Vector2) -> void:
	mouse_interpolation = 0.0
	is_mouse_automoving = true
	automove_pos = pos


func stop_mouse_automove() -> void:
	is_mouse_automoving = false
