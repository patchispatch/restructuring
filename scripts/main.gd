extends Node2D

@onready var background := $Background
@onready var progress_bar := $ProgressBar
@onready var startup_sound := $StartupSound
@onready var background_fans := $BackgroundFans
@onready var startup_timer := $StartupTimer
@onready var startup_screen := $StartupScreen
@onready var progress_var_pop := $ProgressBarPop

var startup_screen_time := 5
var progress_pop_time := 2

# Called when the node enters the scene tree for the first time.
func _ready():
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), 0.0)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Background"), 0.0)
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	boot()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	quit_on_esc()

func _on_progress_bar_animation_finished() -> void:
	remove_child(progress_bar)
	add_child(load(SceneTransition.day_to_load).instantiate())


func _on_startup_sound_finished() -> void:
	remove_child(startup_screen)
	background_fans.play()
	background.visible = true
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	progress_var_pop.wait_time = progress_pop_time
	progress_var_pop.start()
	await progress_var_pop.timeout
	progress_bar.visible = true
	progress_bar.play("default")


func boot() -> void:
	startup_sound.play()
	startup_timer.wait_time = startup_screen_time
	startup_timer.start()
	await startup_timer.timeout
	startup_screen.visible = true


func quit_on_esc():
	if Input.is_action_pressed("exit"):
		get_tree().quit()
