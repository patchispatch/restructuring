extends TextureRect

@export var regular_texture: Texture2D
@export var selected_texture: Texture2D

var is_selected := false

signal profile_selected
signal profile_deselected


# Called when the node enters the scene tree for the first time.
func _ready():
	self.texture = regular_texture


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	self.texture = selected_texture if is_selected else regular_texture


func _input(event):
	if not MouseSpriteController.is_dialog_showing:
		handle_left_click(event)


func is_clicked(mouse_pos: Vector2) -> bool:
	return self.get_rect().has_point(mouse_pos)


func handle_left_click(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.pressed and not event.is_echo() and event.button_index == MOUSE_BUTTON_LEFT:
		event = make_input_local(event)
		if is_clicked(event.position):
			# Switch selection
			is_selected = not is_selected
			self.get_viewport().set_input_as_handled() # if you don't want subsequent input callbacks to respond to this input
			
			if is_selected:
				profile_selected.emit()
			else:
				profile_deselected.emit()
