extends Control

var profile_id: String

@onready var profile_name = $ProfileName
@onready var skill_1 = $Skill1
@onready var skill_2 = $Skill2
@onready var pic = $Window/ProfilePic

signal profile_selected(profile_id: String)
signal profile_deselected(profile_id: String)


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func initialize(profile_name: String, skill_1: String, skill_2: String, pic_code: String) -> void:
	self.profile_id = pic_code
	self.profile_name.text = profile_name
	self.skill_1.text = skill_1
	self.skill_2.text = skill_2
	pic.texture = ResourceLoader.load("res://resources/sprites/profile-pics/%s.png" % pic_code)


func _on_profile_window_profile_selected() -> void:
	profile_selected.emit(profile_id)


func _on_profile_window_profile_deselected() -> void:
	profile_deselected.emit(profile_id)
