extends Node


@onready var prof_1 = $ProfileWindow1

@export var profile_batch_path: String

signal profile_selected(profile_id: String)
signal profile_deselected(profile_id: String)


# Called when the node enters the scene tree for the first time.
func _ready():
	var profile_batch = get_profiles()
	assert(profile_batch, "Profile batch not loaded properly.")
	initialize_profile_batch(profile_batch)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_profile_window_profile_selected(profile_id: String) -> void:
	profile_selected.emit(profile_id)


func _on_profile_window_profile_deselected(profile_id: String) -> void:
	profile_deselected.emit(profile_id)


func get_profiles() -> Array:
	assert(FileAccess.file_exists(profile_batch_path), "File path does not exist")
	
	var file := FileAccess.open(profile_batch_path, FileAccess.READ)
	var json_file := JSON.new()
	var parse_result := json_file.parse(file.get_as_text())

	if parse_result == OK and typeof(json_file.get_data()) == TYPE_ARRAY:
		return json_file.get_data()
	else:
		return []


func initialize_profile_batch(profile_batch: Array) -> void:
	var profiles = [prof_1]
	var prof = profiles[0]
	var pf = profile_batch[0]
	prof.initialize(pf.name, pf.skill_1, pf.skill_2, pf.pic_code)

