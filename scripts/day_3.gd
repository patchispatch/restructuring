extends DayManager

@export var objective := 3

var view_nuria_dialog := false
var view_chris_dialog := false
var view_nannerl_dialog := false
var view_karim_dialog := false

# Called when the node enters the scene tree for the first time.
func _ready():
	super()
	objective_count.objective = objective
	objective_count.visible = false
	submit_button.visible = false
	load_dialog("day_3_welcome")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func act_on_profile_selected(profile_id: String) -> void:
	super(profile_id)
	
	# React for specific profile
	match profile_id:
		"03_01_1":
			if not view_nuria_dialog:
				view_nuria_dialog = true
				load_dialog("day_3_nuria")
		"03_01_2":
			if not view_chris_dialog:
				view_chris_dialog = true
				load_dialog("day_3_chris")
		"03_01_3":
			if not view_nannerl_dialog:
				view_nannerl_dialog = true
				load_dialog("day_3_nannerl")
		"03_01_4":
			if not view_karim_dialog:
				view_karim_dialog = true
				load_dialog("day_3_karim")



func act_on_profile_deselected(profile_id: String) -> void:
	super(profile_id)
	
	# React for specific profile
	pass


func act_on_dialog_finish(dialog_code: String) -> void:
	match dialog_code:
		"day_3_welcome":
			load_profile_batch("day_3_pb")
		"day_3_end":
			check_on_finish_conditions()


func act_on_submit_button_pressed() -> void:
	if objective_count.count >= objective:
		load_dialog("day_3_end")
	else:
		load_dialog("objective_missing")


func check_on_finish_conditions() -> void:
	SceneTransition.change_scene("res://scenes/day_4.tscn")
