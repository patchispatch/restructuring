extends DayManager

@export var objective := 2

var view_agnes_dialog := false
var view_harley_dialog := false
var view_don_dialog := false
var view_intern_dialog := false

# Called when the node enters the scene tree for the first time.
func _ready():
	super()
	objective_count.objective = objective
	objective_count.visible = false
	submit_button.visible = false
	load_dialog("day_1_welcome")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func act_on_profile_selected(profile_id: String) -> void:
	super(profile_id)
	
	# React for specific profile
	match profile_id:
		"01_01_1":
			if not view_agnes_dialog:
				view_agnes_dialog = true
				load_dialog("day_1_agnes")
		"01_01_2":
			if not view_intern_dialog:
				view_intern_dialog = true
				load_dialog("day_1_intern")
		"01_01_3":
			if not view_harley_dialog:
				view_harley_dialog = true
				load_dialog("day_1_harley")
		"01_01_4":
			if not view_don_dialog:
				view_don_dialog = true
				load_dialog("day_1_don")



func act_on_profile_deselected(profile_id: String) -> void:
	super(profile_id)
	
	# React for specific profile
	pass


func act_on_dialog_finish(dialog_code: String) -> void:
	match dialog_code:
		"day_1_welcome":
			load_profile_batch("day_1_pb")
		"day_1_end":
			check_on_finish_conditions()


func act_on_submit_button_pressed() -> void:
	if objective_count.count >= objective:
		load_dialog("day_1_end")
	else:
		load_dialog("objective_missing")


func check_on_finish_conditions() -> void:
	if selected_profiles.has("01_01_2"):
		SceneTransition.change_scene("res://scenes/day_4.tscn")
	else:
		SceneTransition.change_scene("res://scenes/day_2.tscn")
