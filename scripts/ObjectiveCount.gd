extends Label

const TEXT_TEMPLATE = "SELECTED: {current}/{total}"

var objective: int
var count: int = 0:
	set(value):
		count = value
		_update_text()


func _update_text() -> void:
	text = TEXT_TEMPLATE.format({"current": count, "total": objective})
